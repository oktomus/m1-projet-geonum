#include "HalfEdge.hpp"
#include "Mesh.hpp"
#include "Vertex.hpp"

#include <memory>
#include <cmath>

int main(int argc, const char* argv[]){

	if(argc < 3)
	{
		printf("Usage: %s [-dual|-gc] off_path off_export_path\n", argv[0]);
		printf("\t-dual\tProcess the dual mesh of the given mesh\n");
		printf("\t-gc\tCalculate the gaussian curvate of the mesh. The exported mesh will be colored.\n");
		return 2;
	}

	std::string pin = argv[argc - 2];
	std::string pout = argv[argc - 1];
	bool do_dual = false, do_gc = false;

    // Read flags
	for(int i = 1; i < argc - 2; i ++){
		std::string flag = argv[i];
		if(flag.compare("-dual") == 0)
			do_dual = true;
		else if(flag.compare("-gc") == 0)
			do_gc = true;
	}

	fprintf(stdout, "INPUT: %s\n", pin.c_str());
    fprintf(stdout, "DUAL: %s\n", do_dual ? "true" : "false");
    fprintf(stdout, "GUASSIAN CURVATURE: %s\n", do_gc ? "true" : "false");
	fprintf(stdout, "OUTPUT: %s\n", pout.c_str());

	// Process
	Mesh m;
	m.import(pin);

	//m.taubinSmoothing(150, 0.5f, 0.52f);
	//m.laplacianSmoothing(50, 0.5f);

	/*
	Mesh out;

	if (do_dual) out = m.dual();
	else out = m;

	if (do_gc)
	{
		// Process
		out.exportWithColors(pout);
	}
	else
	{
	}
	*/
    m.processGaussianCurvature();
	m.exportToOFF(pout,true);


	fprintf(stdout, "EXPORT FINISHED\n");
	return 0;
}

#ifndef __VERTEX_HPP__
#define __VERTEX_HPP__

#include "Vector.h"

#include <iostream>
#include <memory>

class Mesh;
class HalfEdge;
typedef std::shared_ptr<HalfEdge> sHalfEdge;

class Vertex {

	friend Mesh;

public:

	Vertex(float x, float y, float z, unsigned int id) :
	    id(id),
	    position(x, y, z)
	{
	}

    Vertex(float x, float y, float z) :
        id(0),
	    position(x, y, z)
    {
    }

    float barycentricArea() const;

    float voronoiArea() const;

	float gaussianCurvature(int = 1) const;

	float medianCurvature(int = 1) const;

	Vector3 uniformLaplacian() const;

	Vector3 cotangentLaplacian() const;


//private:

	/**
	 * Demi arrete incidente, dont la vertex est source
	 */
	sHalfEdge edge;

	/**
	 * Coordonnees
	 */
	Vector3 position;

	float r, g, b, a;

	/**
	 * Identifiant unique
	 */
	unsigned int id;

};

typedef std::shared_ptr<Vertex> sVertex;

#endif

#ifndef __MESH_HPP__
#define __MESH_HPP__

#include "HalfEdge.hpp"
#include "Vertex.hpp"
#include "Face.hpp"

#include <vector>
#include <map>
#include <string>
#include <stdexcept>
#include <fstream>
#include <memory>
#include <glm/vec3.hpp>
#include <float.h>
#include <limits>

typedef std::map<std::pair<unsigned int, unsigned int>, sHalfEdge> hedge_collection;

/**
 * @brief hedge_update_opposite
 * Add in the given collection the given hedge
 * And update hedge opposite hedge if it already exists in the collection
 * @param hedges
 * @param ida
 * @param idb
 * @param hedege_a_to_b
 */
inline void hedge_update_opposite(hedge_collection & hedges, unsigned int ida,
                                  unsigned int idb, sHalfEdge hedge_a_to_b)
{
    hedges.insert(
                std::pair<std::pair<unsigned int, unsigned int>, sHalfEdge>(
                    std::make_pair(ida, idb), hedge_a_to_b));
	// Look for the oposite
    auto it = hedges.find(std::make_pair(idb, ida));
    if(it != hedges.end())
    {
        it->second->setOpposite(hedge_a_to_b);
        hedge_a_to_b->setOpposite(it->second);
    }

}

inline float map(float val, float in_start, float in_end, float out_start, float out_end)
{
	return out_start + ((out_end - out_start) / (in_end - in_start)) * (val - in_start);
}

class Mesh {

public:

	void laplacianSmoothing(size_t iterations, float lambda)
	{
		sVertex vcurrent;
		for(size_t it = 0; it < iterations; it++)
		{
            for(size_t iv = 0; iv < vertices.size(); iv++)
            {
                vcurrent = vertices[iv];
                Vector3 weight = vcurrent->uniformLaplacian();
				vcurrent->position += lambda * weight;
            }
		}
	}

	void taubinSmoothing(size_t iterations, float lambda, float mu)
	{
		sVertex vcurrent;
		bool alternate = true;
		for(size_t it = 0; it < iterations; it++)
		{
            for(size_t iv = 0; iv < vertices.size(); iv++)
            {
                vcurrent = vertices[iv];
                Vector3 weight = vcurrent->cotangentLaplacian();
				if(alternate)
                    vcurrent->position += lambda * weight;
				else
                    vcurrent->position -= mu * weight;
            }
			alternate  = !alternate;
		}
	}

	void processGaussianCurvature()
	{
		sVertex vcurrent;
		float min, max;
		float threshold = 1000;
		bool gmin = false, gmax = true;
		for(size_t iv = 0; iv < vertices.size(); iv++)
		{
			vcurrent = vertices[iv];
			float courbature = vcurrent->gaussianCurvature(2);
			if(courbature > threshold || courbature < -threshold ||
			   courbature != courbature || courbature == std::numeric_limits<float>::infinity())
			{
				std::cout << "NAN COURBATURE :( \n";
				vcurrent->g = 1000;
				vcurrent->b = 0;
			}
			else
			{
                if(gmin || courbature < min )
				{
					gmin = false;
					min = courbature;
				}
                if(gmax || courbature > max )
				{
					gmax = false;
					max = courbature;
				}
                vcurrent->b = courbature;
			}
		}

		std::cout << "MAX: " << max << std::endl;
		std::cout << "MIN: " << min << std::endl;

		float courb;
		for(size_t iv = 0; iv < vertices.size(); iv++)
		{
			vcurrent = vertices[iv];
			courb = vcurrent->b;
            //vcurrent->b = map(
			                  //courb,
			                  //min, min + (max - min) * 0.5f, 0, 1);
			                  //min, max, 0, 1);
			//vcurrent->b = (courb + min) * 0.00001;
			//vcurrent->b = courb;
			vcurrent->g = 0;
			vcurrent->b = (courb-min)/(max-min);
			vcurrent->r = (max-courb)/(max-min);
			//vcurrent->r = map(
			                  //courb,
			                  //min, max, 1, 0);
			//vcurrent->b = 255;
			vcurrent->a = 1;
			//vcurrent->r = 0;
			//vcurrent->g = 1;
			//vcurrent->b = 0;
			//float offset = map(vcurrent->b, 0, 255, 0, 1) / 2.0 + 0.5;
			//vcurrent->g = offset * vcurrent->b;
			//vcurrent->r = 255;
			//vcurrent->r = map(vcurrent->b, 0, 255, 255, 0);
		}

	}

	Mesh dual()
	{
		Mesh mres;

		// Do vertices

		// Store center for each face
		std::map<Face *, sVertex> face_centers;

		// An id that well be never used
		unsigned int fakeId = faces.size() + 100;


        // Calcul centers
		{
            sFace fcurrent = 0;
            sHalfEdge hcurrent, hfirst;
			sVertex scurrent;
            float sumx, sumy, sumz;
            int count_neigh;

            for(int f = 0; f < faces.size(); f++)
            {
                fcurrent = faces.at(f);
                count_neigh = 0;
                sumx = sumy = sumz = 0;

                hfirst = hcurrent = fcurrent->edge;
                do
                {
					scurrent = hcurrent->source;
					sumx += scurrent->position.x;
					sumy += scurrent->position.y;
					sumz += scurrent->position.z;

                    count_neigh++;
                    hcurrent = hcurrent->next;
                } while(hcurrent != hfirst);

                face_centers.insert(std::make_pair(fcurrent.get(),
                                                   std::make_shared<Vertex>(
                                                       sumx / count_neigh,
                                                       sumy / count_neigh,
                                                       sumz / count_neigh,
                                                       fakeId)
                                    ));
            }
		}

		// Create faces
		{
            hedge_collection edges;
            int vertId = 0, count_neigh;
            sHalfEdge  haddcurrent,  haddlast,  haddfirst;
            sHalfEdge  hcurrent,  hlast,  hfirst;
            sVertex  vctrcurrent,  vctrlast,  vctrfirst,  vcurrent;
			sFace  fcurrent;

            for(int v = 0; v < vertices.size(); v++)
            {
                // Get the vertex
                vcurrent = vertices.at(v);

                // Check if this vertex has enough neighbours
                hfirst = hcurrent = vcurrent->edge;
                count_neigh = 0;
                do
                {
                    count_neigh++;
					if(!hcurrent->opposite.get())
					{
						count_neigh = 0; // Border
						break;
					}
                    hcurrent = hcurrent->opposite->next;
                } while(hcurrent != hfirst);
                if(count_neigh < 3) continue;

                fcurrent = std::make_shared<Face>();
                mres.faces.push_back(fcurrent);

                // For each face on this vertex
                hfirst = hcurrent = vcurrent->edge;
                do
                {
                    // Get the center
                    vctrcurrent = face_centers[hcurrent->face.get()];
					if(vctrcurrent->id == fakeId)
					{
                        vctrcurrent->id = vertId++;
                        // Add center to dual mesh summits
                        mres.vertices.push_back(vctrcurrent);
					}

                    // Create half-edge-dual /
                    haddcurrent = std::make_shared<HalfEdge>(vctrcurrent);
                    vctrcurrent->edge = haddcurrent;
                    haddcurrent->face = fcurrent;

                    // If first add
                    if(hcurrent == hfirst)
                    {
                        haddfirst = haddcurrent;
                        vctrfirst = vctrcurrent;
                    }
                    else
                    {
                        haddcurrent->previous = haddlast;
                        haddlast->next = haddcurrent;
                        hedge_update_opposite(edges, vctrlast->id, vctrcurrent->id, haddlast);
                    }

                    vctrlast = vctrcurrent;
                    haddlast = haddcurrent;
                    hcurrent = hcurrent->opposite->next;
                } while(hcurrent != hfirst);

                // Loop edges
                haddfirst->previous = haddcurrent;
                haddcurrent->next = haddfirst;
                hedge_update_opposite(edges, vctrcurrent->id, vctrfirst->id, haddcurrent);
                fcurrent->edge = haddfirst;
            }


		}

		return mres;
	}

	void import(std::string path) {
		std::ifstream offfile(path);
		if(!offfile.is_open()) throw std::runtime_error("Can't open file for reading");

		// Read start process
		vertices.clear();
		faces.clear();

		// Read header
		std::string readStr;
		int readInt;
		float readFloat;

		// Check file type
		offfile >> readStr;
		if(readStr.compare("OFF") != 0)
		{
			offfile.close();
			throw std::runtime_error("This is not an OFF file");
		}

		// Check vert/face count
		int vertCount, faceCount;
		offfile >> vertCount >> faceCount >> readInt; // Line ends with 0
		fprintf(stdout, "Vertices count: %d\nFaces count: %d\n", vertCount, faceCount);

		// Read vertices
		{
			int vertId = 0;
			float x, y, z;
			Vertex * vert = 0;
			HalfEdge * hedge = 0;
			while(vertId < vertCount)
			{
				offfile >> x >> y >> z;
				vertices.push_back(std::make_shared<Vertex>(x, y, z, vertId));
				vertId++;
			}
		}

		// Read faces
		{

			hedge_collection edges;

			int faceId = 0;
			sHalfEdge hcurrent, hlast, hfirst;
			sVertex vcurrent, vlast, vfirst;
			int icurrent, ilast, ifirst;
			sFace face;
			while(faceId < faceCount)
			{
				face = std::make_shared<Face>();
				faces.push_back(face);

				// Get edges
				offfile >> readInt;
				for(int i = 0, n = readInt; i < n; i++)
				{
					offfile >> icurrent;
					vcurrent = vertices.at(icurrent);
					hcurrent = std::make_shared<HalfEdge>(vcurrent);
					vcurrent->edge = hcurrent;
					hcurrent->face = face;

					if(i != 0)
					{
						hcurrent->previous = hlast;
						hlast->next = hcurrent;
						hedge_update_opposite(edges, ilast, icurrent, hlast);
					}
					else
					{
						hfirst = hcurrent;
						vfirst = vcurrent;
						ifirst = icurrent;
					}

					hlast = hcurrent;
					vlast = vcurrent;
					ilast = icurrent;
				}
				// Loop edges
				hfirst->previous = hcurrent;
				hcurrent->next = hfirst;
                hedge_update_opposite(edges, icurrent, ifirst, hcurrent);
				face->edge = hfirst;
				faceId++;
			}
		}
	}

    void exportToOFF(std::string path, bool exportColors = false)
	{
		std::ofstream outf(path);
		if(!outf.is_open()) throw std::runtime_error("Can't open file for writing");
		if(exportColors)
		    outf << "COFF\n";
		else
		    outf << "OFF\n";
		outf << vertices.size() << " "
			<< faces.size() << " "
			<< "0\n";

		outf << std::fixed;

		for(int i = 0; i < vertices.size(); i++)
		{
			sVertex vert = vertices[i];
            outf << vert->position.x << " "
                 << vert->position.y << " "
                 << vert->position.z;
			if(exportColors)
			{
                outf << " "
                     << vert->r << " "
                     << vert->g << " "
                     << vert->b << " "
                     << vert->a;
            }
			outf << "\n";
		}

		std::string indices = "";
        int count = 1;
		sFace face = 0;
		sHalfEdge hedge, firsthedge;
		for(int i = 0; i < faces.size(); i++)
		{
			indices = "";
			count = 0;

			face = faces.at(i);
			hedge = face->edge;
            firsthedge = hedge;
			// Search all edges
			do
			{
				indices += " " + std::to_string(hedge->source->id);
				count++;
				hedge = hedge->next;
			}
			while(hedge != firsthedge);

			outf << std::to_string(count) << " " << indices << "\n";
		}

		outf.close();

	}

	std::vector<sVertex> vertices;
	std::vector<sFace> faces;

};

#endif

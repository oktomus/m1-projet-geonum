#include "Vertex.hpp"
#include "HalfEdge.hpp"

#define PI 3.14159
#define PI2 6.28318530718
#define HPI 1.57079632679

#include <cmath>
#include <iostream>


float Vertex::barycentricArea() const
{
    float area = 0.0f;

    sHalfEdge hfirst, hcurrent;
    std::shared_ptr<Vertex> vleft, vright;

    // For each face
    hcurrent = hfirst = edge;

	do
	{
		// Process edge barycentric area
        float count = 3; // Number of point

		vleft = hcurrent->previous->source;
		vright = hcurrent->next->source;

        // Calculate center of triangle
        Vector3 center = (vleft->position + vright->position + position) / count;

        Vector3 half_vleft = (position + 0.5f * (vleft->position - position));
		Vector3 half_vright = (position + 0.5f * (vright->position - position));

		area += Vector3::triangleArea(position, center, half_vleft);
		area += Vector3::triangleArea(position, center, half_vright);

		if (!hcurrent->opposite) break; // For borders
		hcurrent = hcurrent->opposite->next;
	} while(hcurrent != hfirst);

    return area;
}

float Vertex::voronoiArea() const
{
    float area = 0;

    sHalfEdge hfirst, hcurrent;
    std::shared_ptr<Vertex> vj, va, vb;

    // For each face
    hcurrent = hfirst = edge;

	do
	{
		// Process voronoi area
		vj = hcurrent->next->source;

		va = hcurrent->previous->source;
		if (!hcurrent->opposite) break; // For borders
		vb = hcurrent->opposite->previous->source;

		float alpha = Vector3::angle(position, va->position, vj->position),
		        beta = Vector3::angle(position, vb->position, vj->position);

		area += (tan(HPI - alpha) + tan(HPI - beta)) * position.squaredDistanceTo(vj->position);

		hcurrent = hcurrent->opposite->next;
	} while(hcurrent != hfirst);

	return (1.0/8.0) * area;
}

float Vertex::gaussianCurvature(int method) const
{
	float area;
	switch (method) {
		case 1:
		default:
			area = barycentricArea();
			break;
		case 2:
			area = voronoiArea();
			break;
	}

	double angles = 0.0;

    // For each face
    sHalfEdge hfirst, hcurrent;
    std::shared_ptr<Vertex> va, vb;
    hcurrent = hfirst = edge;

	//std::cout << "Processing angles\n";
	do
	{
		// Process voronoi area
		va = hcurrent->next->source;
		vb = hcurrent->previous->source;

		float cangle =Vector3::angle(va->position, position, vb->position);
		if(hcurrent == hfirst)
            angles = cangle;
		else
            angles += cangle;

		if (cangle != cangle)
		{
			std::cout << "NAN ANGLE :( \n";

		}
		//std::cout << "ANGLE : " << cangle << "\n";

		if (!hcurrent->opposite) break; // For borders
		hcurrent = hcurrent->opposite->next;
	} while(hcurrent != hfirst);

	if(angles != angles)
	{
		std::cout << "NAN ANGLE !!!!\n";
		angles = 0.0;
	}

	//return angles;
	return (1.0f / area) * (PI2 - angles);

}

float Vertex::medianCurvature(int method) const
{
	float area;
	switch (method) {
		case 1:
		default:
			area = barycentricArea();
			break;
		case 2:
			area = voronoiArea();
			break;
	}

	float acurrent;

    // For each face
	Vector3 vsum(0, 0, 0);
    sHalfEdge hfirst, hcurrent;
    sVertex va, vb, xj;
    hcurrent = hfirst = edge;

	do
	{
		if(!hcurrent->opposite){
			std::cout << "Unclosed mesh.\n";
			break;
		}

		// alpha
		va = hcurrent->opposite->previous->source;
		// beta
		vb = hcurrent->previous->source;
		// Xj
		xj = hcurrent->next->source;

		acurrent = tan(HPI - Vector3::angle(position, va->position, xj->position)) +
                   tan(HPI - Vector3::angle(xj->position, vb->position, position));

		if(acurrent != acurrent)
		{
			std::cout << "ANGLE NAN\n";
		}

		if(hcurrent == hfirst)
            vsum = (acurrent * (position - xj->position));
        else
            vsum += (acurrent * (position - xj->position));

		hcurrent = hcurrent->opposite->next;
	} while(hcurrent != hfirst);

	return (1.0f / (4.0 * area)) * vsum.magnitude();

}

Vector3 Vertex::uniformLaplacian() const
{
    Vector3 vsum = edge->next->source->position;

	sHalfEdge hcurrent = edge,
	        hfirst = edge;

	size_t nvert = 1;
	while(hcurrent->opposite && hcurrent->opposite->next != hfirst)
	{
		hcurrent = hcurrent->opposite->next;
		vsum += hcurrent->next->source->position;
		nvert++;
	}

	return (1.0 / (float) nvert) * vsum - position;
}

Vector3 Vertex::cotangentLaplacian() const
{
	float sumangle = 0,
	        acurrent;

    // For each face
	Vector3 vsum(0, 0, 0);
    sHalfEdge hfirst, hcurrent;
    sVertex va, vb, xj;
    hcurrent = hfirst = edge;

	do
	{
		if(!hcurrent->opposite){
			std::cout << "Unclosed mesh.\n";
			break;
		}

		// alpha
		va = hcurrent->opposite->previous->source;
		// beta
		vb = hcurrent->previous->source;
		// Xj
		xj = hcurrent->next->source;

		acurrent = tan(HPI - Vector3::angle(position, va->position, xj->position)) +
                   tan(HPI - Vector3::angle(xj->position, vb->position, position));

		if(acurrent != acurrent)
		{
			std::cout << "ANGLE NAN\n";
		}

		sumangle += acurrent;

		if(hcurrent == hfirst)
            vsum = (acurrent * (xj->position - position));
        else
            vsum += (acurrent * (xj->position - position));

		hcurrent = hcurrent->opposite->next;
	} while(hcurrent != hfirst);

	if(vsum.x != vsum.x || vsum.y != vsum.y || vsum.z != vsum.z)
	{
		std::cout << "VEC NAN\n";
	}
	if(sumangle == 0.0f)
	{
		std::cout << "NUL ANGLE SUM\n";
		return Vector3(1, 1, 1);
	}
	return (1.0 / sumangle) * vsum;
}

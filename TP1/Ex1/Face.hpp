#ifndef __FACE_HPP__
#define __FACE_HPP__

class HalfEdge;
class Mesh;
typedef std::shared_ptr<HalfEdge> sHalfEdge;

class Face {

	friend Mesh;

private:

	/**
	 * Demi arrete incidente
	 */
	sHalfEdge edge;

};

typedef std::shared_ptr<Face> sFace;

#endif

#ifndef __HALF_EDGE_HPP__
#define __HALF_EDGE_HPP__

#include "Vertex.hpp"
#include "Face.hpp"

class Mesh;

class HalfEdge {

	friend Mesh;
    friend Vertex;

public:

	HalfEdge(sVertex source) :
		source(source)
	{
	}

	void setOpposite(sHalfEdge hop)
	{
		opposite = hop;
	}

//private:
    sVertex source;
	sFace face;
	std::shared_ptr<HalfEdge> previous, next, opposite;

};

typedef std::shared_ptr<HalfEdge> sHalfEdge;

#endif
